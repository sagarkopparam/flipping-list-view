package com.example.activity;

import java.util.ArrayList;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.controller.BookstoreClient;
import com.example.controller.ServiceClient;
import com.example.db.CityNames;
import com.example.db.RowItems;
import com.example.flippinglistview.R;

public class DataActivity extends Activity implements OnClickListener,
		OnItemClickListener {

	Button additem, flipview;
	ServiceClient client;

	ListView mEnglishList;
	ListView mFrenchList;

	ArrayList<RowItems> city;
	ArrayList<String> cityNames;
	ArrayList<String> stateNames;

	ArrayAdapter<String> adapterEn;
	ArrayAdapter<String> adapterFr;

	private Interpolator accelerator = new AccelerateInterpolator();
	private Interpolator decelerator = new DecelerateInterpolator();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		client = BookstoreClient.getInstance(this);

		additem = (Button) findViewById(R.id.button_additem);
		flipview = (Button) findViewById(R.id.button_flipview);

		mEnglishList = (ListView) findViewById(R.id.list_en);
		mFrenchList = (ListView) findViewById(R.id.list_fr);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	protected void onResume() {

		getDataforView();

		additem.setOnClickListener(this);
		flipview.setOnClickListener(this);

		mEnglishList.setOnItemClickListener(this);
		mFrenchList.setOnItemClickListener(this);

		super.onResume();
	}

	private void getDataforView() {
		// TODO Auto-generated method stub

		city = new ArrayList<RowItems>();
		cityNames = new ArrayList<String>();
		stateNames = new ArrayList<String>();

		city = client.getCityNames();
		// state = client.getStateNames();

		for (int i = 0; i < city.size(); i++) {
			RowItems rw = new RowItems();

			rw = city.get(i);

			cityNames.add(rw.getCity());
			stateNames.add(rw.getState());
		}

		
		adapterEn = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, cityNames);
		
		adapterFr = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, stateNames);

		mEnglishList.setAdapter(adapterEn);
		mFrenchList.setAdapter(adapterFr);
		// mFrenchList.setRotationY(-90f);

		mEnglishList.invalidateViews();
		mFrenchList.invalidateViews();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button_additem:
			dataInsertion();
			Toast.makeText(getApplicationContext(), "Item Inserted",
					Toast.LENGTH_SHORT).show();
			break;
		case R.id.button_flipview:
			flipit();
			Toast.makeText(getApplicationContext(), "FLIP VIEW",
					Toast.LENGTH_SHORT).show();
			break;

		}
	}

	private void dataInsertion() {
		// TODO Auto-generated method stub

		ContentValues values = new ContentValues();

		values.put(CityNames._CITYNAME, "KOLKATA");
		values.put(CityNames._STATENAME, "WEST BENGAL");

		client.insertData(values);

		getDataforView();

	}

	private void flipit() {
		final ListView visibleList;
		final ListView invisibleList;
		if (mEnglishList.getVisibility() == View.GONE) {
			visibleList = mFrenchList;
			invisibleList = mEnglishList;
		} else {
			invisibleList = mFrenchList;
			visibleList = mEnglishList;
		}
		ObjectAnimator visToInvis = ObjectAnimator.ofFloat(visibleList,
				"rotationY", 0f, 90f);
		visToInvis.setDuration(500);
		visToInvis.setInterpolator(accelerator);
		final ObjectAnimator invisToVis = ObjectAnimator.ofFloat(invisibleList,
				"rotationY", -90f, 0f);
		invisToVis.setDuration(500);
		invisToVis.setInterpolator(decelerator);
		visToInvis.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator anim) {
				visibleList.setVisibility(View.GONE);
				invisToVis.start();
				invisibleList.setVisibility(View.VISIBLE);
			}
		});
		visToInvis.start();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

		ShowAlert(arg0.getContext(), "Do you want to delete the item?", city
				.get(arg0.getPositionForView(arg1)).getId());

	}

	private void ShowAlert(Context context, String string, String arg2) {
		// TODO Auto-generated method stub

		final String data = arg2;

		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.create();
		alertDialog.setTitle("Delete the item");
		alertDialog.setButton("Delete", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				try {

					client.deleteData(data);
					getDataforView();
					
				} catch (Exception e) {

				}
			}
		});
		alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});

		alertDialog.setMessage(string);
		alertDialog.show();
	}

}
