package com.example.activity;

import org.json.JSONArray;
import org.json.JSONException;

import com.example.controller.BookstoreClient;
import com.example.controller.ServiceClient;
import com.example.flippinglistview.R;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

public class SplashScreen extends Activity {

	private ProgressBar progressBar;
	protected ServiceClient     client;
	
	class InitTask extends AsyncTask<Void, Integer, Exception> {

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @SuppressWarnings("static-access")
        @Override
        protected Exception doInBackground(Void... params) {
            // 1.) Initialize or Bind to the Bookstore client
            // 2.) Initialize the DAO
            // 3.) Load the default settings
            // 4.) Load the data
            // 5.) Initialize the FULL/FAST Sync
            // 100/5
            Integer[] status = new Integer[1];
            int percentage = 0;
            for (int i = 0; i < 5; i++) {
                try {
                    if (i == 0) {
                        client = BookstoreClient.getInstance(SplashScreen.this);
                        
                        
                    } else {
                        new Thread().sleep(1000);
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                percentage = percentage + 20;
                status[0] = percentage;
                publishProgress(status);
            }
     
         /*   if(Utility.isOnline(SplashScreen.this)){
                isOnline= Constants.MSG_INTERNET;
            }
            if(client.isSignedUp()){
                isLoggedIn  = true;
            }*/
            return null;
        }
        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(Exception result) {
            super.onPostExecute(result);
            Intent intent = null;
            intent = new Intent(SplashScreen.this, DataActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        }

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (values != null) {
                int progress = values[0].intValue();
                progressBar.setProgress(progress);
            }

        }

    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		initScreenObjects();
        new InitTask().execute();
	}
	
	/**
     * <P> Used to </P>
     */
    private void initScreenObjects() {
        setContentView(R.layout.store_splash);
         progressBar =(ProgressBar) findViewById(R.id.store_splash_progressbar);
    }

    
    /* (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        
        int progress = progressBar.getProgress();
        initScreenObjects();
        progressBar.setProgress(progress);
        super.onConfigurationChanged(newConfig);
    }


}
