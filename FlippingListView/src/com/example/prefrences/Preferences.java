package com.example.prefrences;

import com.example.controller.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

public class Preferences {

	private static Preferences mVersion;
	private PackageManager mPackageManager;
	private Context mContext;
	private SharedPreferences mVersionPreferences;

	public static Preferences getGoogleAppVersion(Context context) {

		if (mVersion == null) {

			mVersion = new Preferences(context);
			return mVersion;

		} else {
			return mVersion;
		}

	}

	private Preferences(Context context) {
		mContext = context;
		mVersionPreferences = context.getSharedPreferences(
				Constants.VERSION_PREFERENCE, Context.MODE_PRIVATE);
		
		mPackageManager = context.getPackageManager();

	}
	
	public void setCreated(boolean value) {
		
		mVersionPreferences.edit().putBoolean(Constants.CREATED, value).commit();
		
		//Log.d("DAVIS", "GoogleVersionPreferences setCatalogsMigration = "+value);
		
	}

	public boolean isCreated() {
		
		if (mVersionPreferences != null) {
			
			return mVersionPreferences.getBoolean(Constants.CREATED, false);
		}
		return false;
		
	}


}
