package com.example.db;

import com.example.controller.Constants;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteHelper extends SQLiteOpenHelper {

	public SqliteHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(getTableQuery());
	}

	private String getTableQuery() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE " + CityNames.TABLE_NAME + "(");
        builder.append("'" + CityNames._ID + "' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,");
        builder.append("'" + CityNames._CITYNAME + "' VARCHAR,");
        builder.append("'" + CityNames._STATENAME + "' VARCHAR ");
        builder.append(")");
        
        
        return builder.toString();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 
		
	}

}
