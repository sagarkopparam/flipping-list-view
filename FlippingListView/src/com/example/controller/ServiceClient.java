package com.example.controller;

import java.util.ArrayList;

import com.example.db.RowItems;

import android.content.ContentValues;

public interface ServiceClient {

	public void insertData(ContentValues values);

	public ArrayList<RowItems> getCityNames();

	//public ArrayList<String> getStateNames();

	public void deleteData(String pos);
}
