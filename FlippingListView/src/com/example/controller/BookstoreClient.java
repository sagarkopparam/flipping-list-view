package com.example.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.example.db.CityNames;
import com.example.db.RowItems;
import com.example.db.SqliteHelper;
import com.example.prefrences.Preferences;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class BookstoreClient implements ServiceClient {

	private static ServiceClient instance;
	// private Storage storage;
	private Context context;
	private Preferences mPreferences;
	SqliteHelper helper;
	SQLiteDatabase db;

	public static ServiceClient getInstance(Context context) {
		if (instance == null) {
			instance = new BookstoreClient(context);
		}
		return instance;
	}

	private BookstoreClient(Context context) {

		this.context = context;
		mPreferences = Preferences.getGoogleAppVersion(context);
		helper = new SqliteHelper(context, Constants.DATABASE_NAME, null,
				Constants.DATABASE_VERSION);
		db = helper.getWritableDatabase();

		init();

	}

	public void insertData(ContentValues values) {
		db.insert(CityNames.TABLE_NAME, null, values);

	}

	private void init() {
		// TODO Auto-generated method stub

		ContentValues values = null;

		if (!mPreferences.isCreated()) {
			values = new ContentValues();

			values.put(CityNames._CITYNAME, "BANGALORE");
			values.put(CityNames._STATENAME, "KARNATAKA");

			db.insert(CityNames.TABLE_NAME, null, values);

			values.clear();
			values = new ContentValues();

			values.put(CityNames._CITYNAME, "HYDERABAD");
			values.put(CityNames._STATENAME, "TELANGANA");

			db.insert(CityNames.TABLE_NAME, null, values);

			values.clear();
			values = new ContentValues();

			values.put(CityNames._CITYNAME, "MUMBAI");
			values.put(CityNames._STATENAME, "MAHARASTRA");

			db.insert(CityNames.TABLE_NAME, null, values);

			values.clear();
			values = new ContentValues();

			values.put(CityNames._CITYNAME, "CHENNAI");
			values.put(CityNames._STATENAME, "TAMILNADU");

			db.insert(CityNames.TABLE_NAME, null, values);

			values.clear();
			values = new ContentValues();

			values.put(CityNames._CITYNAME, "COCHIN");
			values.put(CityNames._STATENAME, "KERALA");

			db.insert(CityNames.TABLE_NAME, null, values);

			values.clear();
			values = new ContentValues();

			mPreferences.setCreated(true);
		}

	}

	@Override
	public ArrayList<RowItems> getCityNames() {

		ArrayList<RowItems> city = new ArrayList<RowItems>();

		//String[] cityName = { CityNames._ID ,CityNames._CITYNAME, CityNames._STATENAME };

		Cursor c = db.query(CityNames.TABLE_NAME, null, null, null, null,
				null, null);

		if (c != null) {
			while (c.moveToNext()) {
				
				RowItems rw = new RowItems();
				rw.setId(c.getString(c
						.getColumnIndex(CityNames._ID)));
				rw.setCity(c.getString(c
						.getColumnIndex(CityNames._CITYNAME)));
				rw.setState(c.getString(c
						.getColumnIndex(CityNames._STATENAME)));
				city.add(rw);
			}
		}

		return city;
	}

	/*@Override
	public ArrayList<String> getStateNames() {

		ArrayList<String> state = new ArrayList<String>();

		String[] stateName = { CityNames._STATENAME };

		Cursor c = db.query(CityNames.TABLE_NAME, stateName, null, null, null,
				null, null);

		if (c != null) {
			while (c.moveToNext()) {
				state.add(c.getString(c.getColumnIndex(CityNames._STATENAME)));
			}
		}

		return state;

	}
*/
	@Override
	public void deleteData(String pos) {
		// TODO Auto-generated method stub

		String where = CityNames._ID + " = '" + pos + "'";
		db.delete(CityNames.TABLE_NAME, where, null);
	}

}
