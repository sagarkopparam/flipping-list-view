package com.example.controller;

public class Constants {
	
	public static String DATABASE_NAME = "state.sqlite";
	public static int DATABASE_VERSION = 1;
	public static final String VERSION_PREFERENCE = "bb_version";
	public static final String CREATED = "db_created";

}
